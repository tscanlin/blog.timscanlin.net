# [blog.timscanlin.net](http://blog.timscanlin.net)

## Install dependencies

```sh
npm install
```

## Build content

```sh
npm run build:content
```

## Run development server

```sh
npm start
```

## Build for production

```sh
npm run build
```

## TODO

- Add RSS feed support
