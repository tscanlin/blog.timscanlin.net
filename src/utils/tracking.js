const TRACKING_GLOBAL = 'APP_TRACKING_GLOBAL'

// autotrack
// https://github.com/googleanalytics/autotrack
// check out more here https://github.com/googleanalytics/autotrack#plugins
export function gaAutotrack (siteId) {
  if (typeof window !== 'undefined') {
    // eslint-disable-next-line
    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=new Date;

    /* global ga: true */
    ga('create', siteId, 'auto')
    ga('require', 'urlChangeTracker')
    ga('require', 'outboundLinkTracker')

    // now that everything is ready, log initial page
    ga('send', 'pageview')
    return true
  }
}

export function initTracking (siteId) {
  if (typeof window !== 'undefined') {
    if (!window[TRACKING_GLOBAL]) {
      window[TRACKING_GLOBAL] = true
      return gaAutotrack(siteId)
    }
  }
}
