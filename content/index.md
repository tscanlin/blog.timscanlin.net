---
siteTitle: "Tim Scanlin"
description: "Coder, Maker, & Designer"
stylesheets:
  - "https://unpkg.com/tachyons@4.7.4/css/tachyons.min.css"
topLinks:
  - text: "Blog"
    href: "/"
  - text: "About"
    href: "http://timscanlin.net"
  - text: "Github"
    href: "https://github.com/tscanlin/"
backgroundClass: "bg-mid-gray"
copyright: "Tim Scanlin"
siteId: "UA-53115940-2"
---
