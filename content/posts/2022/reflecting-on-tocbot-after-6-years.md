---
title: Reflecting on tocbot after 6 years
layout: Post
date: 2022-02-22
draft: false
---

I first created the open source project [tocbot](https://github.com/tscanlin/tocbot) nearly 6 years ago on Mar 22, 2016. It grew from my passion of working on documentation sites which can often have a lot of content and be difficult to navigate. Tocbot makes this easier by providing a simple library to create and update a Table of Contents (TOC) that shows the current heading being scrolled to. You can check out [the docs](http://tscanlin.github.io/tocbot/) to learn more about it and see it in action.

Originally there was a library called [tocify](http://gregfranko.com/jquery.tocify.js/) that accomplished very similar functionality as tocbot, it was essentially what I wanted. However it had a dependency on jquery AND jquery ui, and requiring both of those is a lot, 100's of kilobytes for what should be simple functionality for my simple use case. So then I decided to create tocbot using native dom methods and having no external dependencies, in total it's about 4kb in size.

Working on this project has been quite a ride. There have been over 170 issues opened, as well as over 100 pull requests closed (most of which merged), and tocbot crossed the 1000 stars threshold a month or so ago (at 1039 currently). I've been absolutely amazed by the amount of community engagement, from people finding bugs and corner cases, to the suggestions and pull requests it is all appreciated and helpful in making this library better for everyone.

One of the more surprising things was hearing about who was using tocbot. Tutorials like [this one from hongkiat](https://www.hongkiat.com/blog/tocbot-table-of-contents/) goes over tocbot and how you can use it on your website. There's also a [Drupal plugin](https://www.drupal.org/project/tocbot), and even a tutorial for [how to use it with the blogging platform Ghost](https://ghost.org/docs/tutorials/adding-a-table-of-contents/). And the Spring framework docs have even [used tocbot](https://github.com/spring-io/spring-doc-resources/blob/master/src/main/resources/js/toc.js#L10-L16) for their documentation site. All of these were created without my knowledge and grew organically (I didn't find out about any of these until a year or two after the fact).

Helping people solve problems is something I am passionate about. I am glad that some people have found tocbot to be helpful and I've really enjoyed the experience of building it with help from the open source community. Thank you to everyone who has used tocbot and who has contributed code changes, issues, questions, or suggestions your input is appreciated no matter how small!

I have some other open source projects too although not as popular as tocbot, check them out if you are interested:

- [next-blog](https://github.com/tscanlin/next-blog) - Simple blogging for next.js
- [css-razor](https://github.com/tscanlin/css-razor) - Remove unused CSS quickly and efficiently
- [processmd](https://github.com/tscanlin/processmd) - Process/convert markdown files to json
- [say4me.com](https://www.say4me.com/) - Simple web app for helping people with speech impairments, code is available on [gitlab](https://gitlab.com/tscanlin/say4me.com).
- [serverless-s3-crud](https://github.com/tscanlin/serverless-s3-crud) - Simple s3 CRUD api using a lambda