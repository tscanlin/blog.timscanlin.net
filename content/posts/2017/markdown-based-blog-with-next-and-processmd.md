---
title: Markdown Based Blog with next.js
layout: Post
date: 2017-06-11
draft: false
---

Ever since next.js added the ability to do static exports I've been exploring the possibility of using it for my blog. I looked for utilities to convert markdown and yaml to json but didn't quite find anything that fit my needs so I built a library called [processmd](https://github.com/tscanlin/processmd). It makes it easy to convert a directory of markdown and / or yaml files to a mirrored directory of json files based on glob patterns using [globby](https://github.com/sindresorhus/globby). This is really useful for blogs or files for static websites since then you can just `require()` the json files with node, which is exactly what I did to create the latest iteration of this website.

Let's take a look at how you can use [processmd](https://github.com/tscanlin/processmd) to help you build your own markdown based blog with [next.js](https://github.com/zeit/next.js).

First, let's install it `npm i -D processmd`, then add this to your `scripts` in package.json:

```js
{
  "build:content": "processmd content/**/*.md --outputDir content --preview 110 --summaryOutput content/summary.json",
}
```

I keep all of the content in a top level `content/` directory separate from source files, but there's no reason they couldn't live side-by-side. Then I add a git ignore rule to ignore all json files in that directory (`content/**/*.json`). This keeps all your content in one place and results in less path mapping confusion.

Then `--preview 110` adds a preview property based on the number of letters in the stripped markdown rounding down to the last word (so you don't get half words).

And finally `--summaryOutput content/summary.json` creates a summary file that's useful for creating and index page for your blog posts. By default it excludes post content but that behavior can be overridden with the `includeBodyProps` option.

Other files that you will need are a couple config files for next.js: [server.js](https://github.com/tscanlin/next-blog/blob/master/server.js), [next.config.js](https://github.com/tscanlin/next-blog/blob/master/next.config.js), check out [/src](https://github.com/tscanlin/next-blog/tree/master/src) for more.

Check out [next-blog](https://github.com/tscanlin/next-blog), an example you can clone and use to build your own markdown based blog powered by next.js.
