---
title: Announcing imgmin and other updates
layout: Post
date: 2023-12-6
draft: false
---

I first started working on imgmin around this time last year and released it in April. So far it has yet to receive a lot of traffic. But I also haven't done much to promote it. Anyway, I think it was a cool project because it was the first time I had made use of WASM, and I've been a big fan of [sharp](https://sharp.pixelplumbing.com/) for a while (and also [libvips](https://www.libvips.org/)) so when I saw that there was a project called [wasm-vips](https://github.com/kleisauke/wasm-vips), I wanted to learn more and see how it worked. That's when I started working on imgmin and seeing if I could make an image compressor / converter entirely client side.

I created a simple next.js based site available at [imgmin.com](https://www.imgmin.com/). You can drag and drop as many photos as you want and it can compress / covert hundreds of images at a time as well as resize them. As opposed to other tools, the images never leave the clients browser and all the processing happens 100% locally.

This tool was really fun to build and I hope to make more improvements. Things I'd like to add are .avif support, ability to see and maybe edit metadata, as well as some data visualizations of some kind. But for now its pretty good and quick and it was interesting to build. I hope people find it useful.

Other projects I've been working on include 3d printing, using [wled](https://kno.wled.ge/) to control LEDs around my home, and supporting my open source projects like [tocbot](https://github.com/tscanlin/tocbot). Huge thanks to my first github sponsors I've gotten this year! You can [check them out here](https://github.com/sponsors/tscanlin).

Looking forward, I'm going to try and publish a repo for 3d print files soon and I have a few other things in the works. I'll share more on that later.
